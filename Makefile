EXECS= multigrid_2d
CC=gcc
FLAGS=-O3 -Wall -g 

all: ${EXECS}

multigrid_2d: multigrid_2d.c
	${CC} ${FLAGS} multigrid_2d.c -lm -lrt -o multigrid_2d 

clean:
	rm -f ${EXECS}
