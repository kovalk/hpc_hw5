/* Multigrid for solving -(uxx+uyy)=f for x in (0,1)^2
 * Usage: ./multigrid_2d < Nfine > < iter > [s-steps]
 * NFINE: number of intervals in each direction on finest level (not including borders - size of full grid is (NFINE+2)^2)
   must be of the form 2^j-1
 * ITER: max number of V-cycle iterations
 * S-STEPS: number of Jacobi smoothing steps; optional
 * Used Georg's 1d multigrid as starting point & the article by Irad Yavneh as reference
 */
#include <stdio.h>
#include <math.h>
#include "util.h"
#include <string.h>

/* compuate norm of residual */
double compute_norm(double *u, int N)
{
  int i;
  double norm = 0.0;
  for (i = 0; i < (N+2)*(N+2); i++)
    norm += u[i] * u[i];
  return sqrt(norm);
}

/* set vector to zero */
void set_zero (double *u, int N) {
  int i;
  for (i = 0; i < (N+2)*(N+2); i++)
    u[i] = 0.0;
}

/* debug function */
void output_to_screen (double *u, int N) {
  int i;
  for (i = 0; i < (N+2)*(N+2); i++)
    printf("%f ", u[i]);
  printf("\n");
}

/* assuming N = 2^j-1, coarsen grid from 
   (N+2)^2 to (2^{j-1}+1)^2
*/
// Note that we keep the boundary values zero 
void coarsen(double *uf, double *uc, int N) {
  int ic;
  int jf = floor(log2(N+1)); // j such that N = 2^j-1 on fine grid
  int NC = (int) (pow(2,jf-1)-1); // Number of discretization points in either direction on the coarse grid
  for (ic = 1; ic <= NC*NC ; ++ic) {
    int posc = NC+ic+2*ceil(1.*ic/NC); // position in uc vector 
    int pos_xc = ceil(1.*ic/(NC));
    int pos_yc = posc-pos_xc*(NC+2); 
    int pos_xf = pos_xc*2;
    int pos_yf = pos_yc*2; 
    int posf = pos_xf*(N+2)+pos_yf; // position corresponding to posc in refined vector
    uc[posc] = (1./16.)*( 4*uf[posf] + 2*( uf[posf+1]+uf[posf-1]+uf[posf+N+2]+uf[posf-(N+2)] ) + ( uf[posf+N+1]+uf[posf+N+3]+uf[posf-(N+2)+1]+uf[posf-(N+2)-1] ) ); 
  }
}

/* assuming N = 2^j-1, refine grid from 
   (N+2)^2 to (2^{j+1}+1)^2
*/
void refine_and_add(double *u, double *uf, int N)
{
  int i;
  int jc = floor(log2(N+1)); // j such that N = 2^j-1 on coarse grid 
  int NF = (int) (pow(2,jc+1)-1); // Number of discretization points in either direction on the fine grid
  //printf("fine mesh is: %d \n \n",NF);
  for (i = 1; i <= N*N; ++i) {
    int posc = N+i+2*ceil(1.*i/N); // position in the coarse grid 
    int pos_xc = ceil(1.*i/(N));
    int pos_yc = posc-pos_xc*(N+2);
    int pos_xf = pos_xc*2;
    int pos_yf = pos_yc*2; 
    int posf = pos_xf*(NF+2)+pos_yf; // poisition corresponding to posc in refined grid 
    // Take care of ourselves on the fine mesh
    uf[posf] += u[posc]; 

    /* Take care of the terms directly above/below us and to the right/left of us on fine
    mesh */  
    uf[posf-1] += 0.5*u[posc];
    uf[posf+1] += 0.5*u[posc];
    uf[posf+NF+2] += 0.5*u[posc];
    uf[posf-(NF+2)] += 0.5*u[posc];

    /* Take care of the diagonal terms on the fine mesh */ 
    uf[posf+NF+3] += 0.25*u[posc];
    uf[posf+NF+1] += 0.25*u[posc];
    uf[posf-(NF+2)+1] += 0.25*u[posc];
    uf[posf-(NF+2)-1] += 0.25*u[posc];


  }
}

/* compute residual vector */
void compute_residual(double *u, double *rhs, double *res, int N, double invhsq)
{
  int i;
  for (i = 1; i <= N*N; i++) { // loop over only inner points since boundary terms are fixed to zero 
    int pos = N+i+2*ceil(1.*i/N);
    res[pos] = (rhs[pos]-invhsq*(4.*u[pos] - (u[pos+1]+u[pos-1]+u[pos+N+2]+u[pos-(N+2)])));
  }
}


/* compute residual and coarsen */
void compute_and_coarsen_residual(double *u, double *rhs, double *resc,
				  int N, double invhsq)
{
  double *resf = calloc(sizeof(double), (N+2)*(N+2));
  compute_residual(u, rhs, resf, N, invhsq);
  coarsen(resf, resc, N);
  free(resf);
}


/* Perform Jacobi iterations on u - only on inner points */
void jacobi(double *u, double *rhs, int N, double hsq, int ssteps)
{
  int i, j;
  /* Jacobi damping parameter -- plays an important role in MG */
  double omega = 2./3.;
  double *unew = calloc(sizeof(double), (N+2)*(N+2));
  for (j = 0; j < ssteps; ++j) { 
    for (i = 1; i <= N*N; i++){ 
      int pos = N+i+2*ceil(1.*i/N);
      unew[pos] = u[pos]+omega*0.25*(hsq*rhs[pos]+u[pos+1]+u[pos-1]+u[pos+N+2]+u[pos-(N+2)]-4*u[pos]);
    }
    memcpy(u, unew, ((N+2)*(N+2))*sizeof(double));
  }
  free (unew);
}


int main(int argc, char * argv[])
{
  int i, Nfine, l, iter, max_iters, levels;
  int ssteps = 3;

  if (argc < 3 || argc > 4) {
    fprintf(stderr, "Nfine: # of intervals, must be 2^j-1 for some int j >= 1\n");
    fprintf(stderr, "Usage: ./multigrid_2d Nfine maxiter [s-steps]\n");
    fprintf(stderr, "s-steps: # jacobi smoothing steps (optional, default is 3)\n");
    abort();
  }
  sscanf(argv[1], "%d", &Nfine);
  sscanf(argv[2], "%d", &max_iters);
  if (argc > 3)
    sscanf(argv[3], "%d", &ssteps);

  /* compute number of multigrid levels = log2(Nf+1) */
  levels = floor(log2(Nfine+1));
  printf("Multigrid Solve using V-cycles for -(u_{xx}+u_{yy}) = f on (0,1)x(0,1)\n");
  printf("Number of intervals = %d, max_iters = %d\n", Nfine*Nfine, max_iters);
  printf("Number of MG levels: %d \n", levels);

  /* timing */
  timestamp_type time1, time2;
  get_timestamp(&time1);

  /* Allocation of vectors, including left and right bdry points */
  double *u[levels], *rhs[levels];
  /* N, h*h and 1/(h*h) on each level */
  int *N = (int*) calloc(sizeof(int), levels);
  double *invhsq = (double* ) calloc(sizeof(double), levels);
  double *hsq = (double* ) calloc(sizeof(double), levels);
  double * res = (double *) calloc(sizeof(double), (Nfine+2)*(Nfine+2));
  for (l = 0; l < levels; ++l) {
    N[l] = (int) (pow(2,levels-l)-1);
    double h = 1.0 / N[l];
    hsq[l] = h * h;
    printf("MG level %2d, N = %8d\n", l, N[l]);
    invhsq[l] = 1.0 / hsq[l];
    u[l]    = (double *) calloc(sizeof(double), (N[l]+2)*(N[l]+2));
    rhs[l] = (double *) calloc(sizeof(double), (N[l]+2)*(N[l]+2));
  }
  /* rhs on finest mesh */
  for (i = 0; i <= (N[0])*(N[0]); ++i) {
    int pos = N[0]+i+2*ceil(1.*i/N[0]);
    rhs[0][pos] = 1.0;
  }
  double res_norm, res0_norm, tol = 1e-6;

  /* initial residual norm */
  compute_residual(u[0], rhs[0], res, N[0], invhsq[0]);
  res_norm = res0_norm = compute_norm(res, N[0]);
  printf("Initial Residual: %f\n", res0_norm); 

  for (iter = 0; iter < max_iters && res_norm/res0_norm > tol; iter++) {
    /* V-cycle: Coarsening */
    for (l = 0; l < levels-1; ++l) {
      /* pre-smoothing and coarsen */
      jacobi(u[l], rhs[l], N[l], hsq[l], ssteps);
      compute_and_coarsen_residual(u[l], rhs[l], rhs[l+1], N[l], invhsq[l]);
      /* initialize correction for solution with zero */
      set_zero(u[l+1],N[l+1]);
    }
    /* V-cycle: Solve on coarsest grid using many smoothing steps */
    jacobi(u[levels-1], rhs[levels-1], N[levels-1], hsq[levels-1], 50);

    /* V-cycle: Refine and correct */
    for (l = levels-1; l > 0; --l) {
      /* refine and add to u */
      refine_and_add(u[l], u[l-1], N[l]);
      /* post-smoothing steps */
      jacobi(u[l-1], rhs[l-1], N[l-1], hsq[l-1], ssteps);
    }
   // printf("post refine \n\n");

    if (0 == (iter % 1)) {
      compute_residual(u[0], rhs[0], res, N[0], invhsq[0]);
      res_norm = compute_norm(res, N[0]);
      printf("[Iter %d] Residual norm: %2.8f\n", iter, res_norm);
    }
  }

  /* Clean up */
  free (hsq);
  free (invhsq);
  free (N);
  free(res);
  for (l = levels-1; l >= 0; --l) {
    free(u[l]);
    free(rhs[l]);
  }

  /* timing */
  get_timestamp(&time2);
  double elapsed = timestamp_diff_in_seconds(time1,time2);
  printf("Time elapsed is %f seconds.\n", elapsed);
  return 0;
}
