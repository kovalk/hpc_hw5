Repo for HPC hw 5

Generalization of jacobi to 2d multigrid - serial version 
Usage: ./multigrid_2d Nfine Max_Iter ssteps
Nfine has to be of the form 2^k-1, and corresponds to the number of points in x and y directions. Doesn't include boundaries

Results for some test runs, ssteps = 5, ran on crunchy1: 
N = 511 (so full grid is 513*513)
Iters for convergence: 14
final residual norm: 0.0004
Time: ~3 seconds

N = 1023 
Iters for convergence: 14 
final residual norm: 0.0008
Time: 12.29 seconds

N = 4095
Iters for convergence: 14
final residual norm: 0.0038
Time: 197.6 seconds


